/**
 * ==========================================
 * App to calculate the size of an directory. 
 * ==========================================
 * @author Dennis Stanglmair
 * @version 1.0.0
 */

/**
 * Model that manages the directory and its data.
 */
class Model {

    root: DirectoryElement                      // root element
    currentSize: number                         // current calculated size, descending from root
    activeFolder: DirectoryElement              // the active folder, where DirectoryElements can currently be added

    /**
     * Initializing variables with default values.
     * Init root with an empty folder. 
     */
    constructor() {
        this.root = new DirectoryFolder()
        this.currentSize = 0
        this.activeFolder = this.root
    }

    /**
     * Reset directory by overwriting the root with an empty folder.
     */
    reset() {
        this.root = new DirectoryFolder()
        this.activeFolder = this.root
    }
    /**
     * Recursivly calculating directory size by callin @function getDirSize from root element.
     */
    calcDirectorySize() {
        this.currentSize = this.root.getDirSize()
    }

    /**
     * Importing a new directory in JSON-Format and calculating new @var currentSize using @function calcDirectorySize
     * @param str Directory in JSON Format as a String
     * @returns root as top level DirectoryElement
     */
    readNewDirectory(str: string): DirectoryElement {
        this.root = this.importJSON(str)
        this.calcDirectorySize()
        return this.root
    }

    /**
     * Recursivly pasing JSON-File into Datastructure using @class DirectoryElemnt.
     * Throws an SyntaxError if JSON-Format is not as expected.
     * @param str Directory in JSON Format as a String
     * @returns top level DirectoryElement
     */
    importJSON(str: string): DirectoryElement {
        let obj = JSON.parse(str);

        if (obj.type == "DirFolder") {
            let folder = new DirectoryFolder();

            for (const child of obj.children) {
                folder.children.push(this.importJSON(child));
            }
            return folder;
        } else if (obj.type == "DirFile") {
            return new DirectoryFile(parseInt(obj.size));
        } else {
            throw new SyntaxError(
                "Syntax Error occured reading JSON. type: " + obj.type + " not expected."
            );
        }
    }

    /**
     * Setting the active folder to a new DirectoryFolder.
     * @param elem The DirectoryFolder that is selected as active.
     */
    setActiveFolder(elem: DirectoryFolder) {
        this.activeFolder = elem
    }


}
/**
 * Abstract class that represents all directory elements and their common traits
 */
abstract class DirectoryElement {
    size: number;           // size of this element 

    constructor() {
        this.size = 0;      // init size with 0
    }

    abstract getDirSize(): number;   // abstract getter for size
    abstract getJSON(): string;      // abstract getter for JSON-Format as string
}

/**
 * Class to represent folders extending @class DirectoryElement.
 * Folder size is always set to 0; Managed by super() constructor
 */
class DirectoryFolder extends DirectoryElement {
    children: DirectoryElement[];          // list of descendents from this folder

    constructor() {
        super();
        this.children = [];                 // init children as an empty list
    }

    /**
     * Calculates size using this DirectoryFolder Element as a root. 
     * @returns size of this directory
     */
    getDirSize(): number {
        let sum: number = 0;

        for (const child of this.children) {
            sum += child.getDirSize();  // recursiv call to get size from children and summing them up
        }
        return sum;
    }

    /**
     * Adding a new child to this folder element
     * @param child child to add: can be DirectoryFolder or DirectoryFile
     */
    addChild(child: DirectoryElement): void {
        this.children.push(child);
    }

    /**
     * Recursivly concatenates the JSON representation of this folder, including the descendants
     * @returns This elements JSON representation as string
     */
    getJSON(): string {
        let childrenObj = [];
        for (const child of this.children) {    // iterate over all children
            childrenObj.push(child.getJSON()); // recursive call to get JSON Representation of children.
        }
        let obj = {
            "type": 'DirFolder',                // type to easily differentiate between folder and files
            children: childrenObj,              // list of children objects
        };

        return JSON.stringify(obj);
    }

}

/**
 * Class to represent files extending @class DirectoryElement.
 * File size is reset by constructor.
 */
class DirectoryFile extends DirectoryElement {


    constructor(size: number) { // constructor need size of this file
        super();
        this.size = size;       // setting size
    }

    /**
     * Getter for this files size. Used as an end to the recusive call.
     * @returns size of this file.
     */
    getDirSize(): number {
        return this.size;
    }

    /**
     * Getter for JSON representation of this file.
     * @returns This elements JSON representation as string
     */
    getJSON(): string {
        return JSON.stringify({
            'type': 'DirFile',
            size: this.size,
        });
    }
}

/**
 * Class to handle UI based actions 
 */
class View {

    upload: HTMLInputElement        // Button to upload JSON File
    ImportButton: HTMLInputElement  // Button to import JSON File
    ClacButton: HTMLInputElement    // Button to calculate directory size
    valueLabel: HTMLElement         // Label to display directory size
    app: Element                    // root element of the calculator module
    editor: Element                 // root element of the editor module
    JSONHeader: Element             // headline of JSON sub-module
    JSONContainter: Element         // container element of JSON sub-module
    JSONTopBar: Element             // JSON sub-module toolbar
    JSONBottomBar: Element          // JSON sub-module toolbar
    LabelBar: Element               // container of size display
    ErrorLine: Element              // container to display error messages
    folderClickHandler: any         // handler to manage folder click
    sizeValue: HTMLInputElement     // size value of potential new file to be added


    constructor(clickHandler: any) {

        this.folderClickHandler = clickHandler // clickhandler for click on folder, changing the new active folder
        
        // Building the UI for the right calculator module inclusive JSON importer
        this.app = document.getElementById('root')!

        // JSON Headline and container
        this.JSONHeader = this.createElement('h2', ["sub-module-header"])
        this.JSONHeader.textContent = "JSON-Reader"

        // JSON import tools
        this.ImportButton = <HTMLInputElement>this.createElement('button', ["button"])
        this.ImportButton.textContent = "Import"

        // Calculate Size tools
        this.ClacButton = <HTMLInputElement>this.createElement('button', ["button"])
        this.ClacButton.textContent = "Calculate Size"

        // Display size tools
        this.valueLabel = this.createElement('div', ["label"])
        this.valueLabel.textContent = "0"
       
        // Upload file tools
        this.upload = <HTMLInputElement>this.createElement('input')
        this.upload.type = "file"
        this.upload.accept = ".txt, .JSON, .json"


        // Error line components
        this.ErrorLine = this.createElement("div", ['error-line'])
        this.ErrorLine.textContent = ''

        // JSON import components
        this.JSONContainter = this.createElement('div', ['sub-module-container'])
        this.JSONTopBar = this.createElement('div', ['sub-module-toolbar'])
        this.JSONBottomBar = this.createElement('div', ['sub-module-toolbar-bottom'])
        this.JSONTopBar.append(this.upload)
        this.JSONBottomBar.append(this.ImportButton)
        this.JSONContainter.append(this.JSONHeader, this.JSONTopBar, this.JSONBottomBar, this.ErrorLine)
        
        // Size display components
        this.LabelBar = this.createElement('div', ['sub-module-toolbar'])
        let Label = this.createElement('span', ['label-bar-label'])
        Label.textContent = "Directory Size:"
        this.LabelBar.append(Label, this.valueLabel)
    
        // Calculating size components
        const calcBar = this.createElement('div', ['sub-module-toolbar'])
        calcBar.append(this.ClacButton)
        const calcHeader = this.createElement('h2', ["sub-module-header", "calcHeader"])
        calcHeader.textContent = "Calculator"

        this.app.append(this.JSONContainter, calcHeader, this.LabelBar, calcBar)


        // Building UI for left directory editor and display module
        this.editor = document.getElementById('rootLeft')!
        this.editor.classList.add('container')

        // Input for setting the size of a potentially new file
        this.sizeValue = <HTMLInputElement>this.createElement('input', ['number-input'])
        this.sizeValue.type = "number"
        this.sizeValue.min = "0"
        this.sizeValue.max = "999"
        this.sizeValue.value = "1"
        this.sizeValue.step = "1"
        document.getElementById("size-container")?.append(this.sizeValue)
    }
    /**
     * Displaying HTML structure for directory
     * @param elem HTML representation of directory as @class Element
     */
    BuildHtml(elem: Element) {
        this.editor.innerHTML = "" // resetting structure
        this.editor.append(elem)   // loading new structure
    }

    /**
     * Get the HTML representation of @param elem 
     * @param elem Element to be translated to HTML
     * @param activeElem currently active element needed to adjust color indicator
     * @returns recursivly built HTML representation of @param elem
     */
    DirectoryElementToHTML(elem: DirectoryElement, activeElem: DirectoryElement): Element {

        let html = this.createElement('div', ['dir-row']) // class dir-row has an left margin, so they get indented, compared to their parent 
        const img = <HTMLImageElement>this.createElement('img', ['dir-icon'])
        html.append(img)
        if (elem instanceof DirectoryFolder) { // create folder html
            img.src = "res/folder.png"
            img.classList.add("tool-icon")
            if (elem == activeElem) img.classList.add('active-folder')
            for (const child of elem.children) {
                html.append(this.DirectoryElementToHTML(child, activeElem))
            }
            img.addEventListener('click', event => {    // eventlistener added to every folder, to set them active
                event.preventDefault()
                event.stopPropagation()
                this.folderClickHandler(elem)
            })
        } else if (elem instanceof DirectoryFile) { // create file html; else possible.
            img.src = "res/file.png"
            let s = this.createElement('span',['file-size-label'])
            s.append(`(${elem.size})`)
            html.append(s) // addinf file size in braces behind every file
        }
        return html
    }

    /**
     * Updating the label to display the current directory size.
     * @param value size value to be displayed
     */
    updateValueLabel(value: number) {
        this.valueLabel.innerHTML = value.toString()
    }
    

    /**
     * Binding handler to new folder icon click.
     * @param handler newFolderClickedHandler 
     */
    bindNewFolder(handler: any) {
        const newFolderButton = document.getElementById("newFolderIcon")
        newFolderButton?.addEventListener('click', event => {
            event.preventDefault()
            handler() 
        })
    }

    /**
     * Binding handler to new file click.
     * @param handler newFileClickedHandler
     */
    bindNewFile(handler: any) {
        const newFileButton = document.getElementById("newFileIcon")
        newFileButton?.addEventListener('click', event => {
            event.preventDefault()
            let finalValue = Math.floor(parseFloat(this.sizeValue.value))
            if (isNaN(finalValue)) return
            if(finalValue<0) finalValue=0
            this.sizeValue.value = finalValue.toString()
            handler(finalValue)
        })
    }

    /**
     * Binding handler to reset click.
     * @param handler resetClickedHandler
     */
    bindReset(handler: any) {
        const resetButton = document.getElementById("resetIcon")
        resetButton?.addEventListener('click', event => {
            event.preventDefault()
            handler()
        })
    }

    /**
     * Binding handler to save click.
     * @param handler saveClickedHandler
     */
    bindSave(handler: any) {
        const saveButton = document.getElementById("saveIcon")
        saveButton?.addEventListener('click', event => {
            event.preventDefault()
            handler()
        })
    }

    /**
     * Binding handler to import click.
     * @param handler handleImport
     */
    bindImport(handler: any) {
        this.ImportButton.addEventListener('click', event => {
            event.preventDefault()

            let fileToLoad = this.upload.files![0]
            if (!fileToLoad) { // handling no file selected
                this.ErrorLine.textContent = "There is no file selected! Please select a .txt or .json File."
                throw new Error("No File selected!")
            } else if (fileToLoad.type != 'text/plain' && fileToLoad.type != 'application/json') { // handling wrong file type selected
                this.ErrorLine.textContent = "The Calculator does not support Files of type " + fileToLoad.type + "! Please select a .txt or .json File."
                throw Error("File Type does not match!")
            } else { // cleansing error line
                this.ErrorLine.textContent = ""
            }
            var fileReader = new FileReader();

            // read file and sending it to handler to import it
            fileReader.onload = function (fileLoadedEvent) {
                console.log(<string>fileLoadedEvent.target!.result)
                handler(<string>fileLoadedEvent.target!.result)
            };

            fileReader.readAsText(fileToLoad, "UTF-8");
        })
    }

    /**
     * Binding handler to calc button click.
     * @param handler handleCalc
     */
    bindCalc(handler: any) {
        this.ClacButton.addEventListener('click', event => {
            event.preventDefault()
            handler()
        })
    }

    /**
     * Create an Element for DOM Tree
     * @param tag Tag of element
     * @param classNames css class names as list
     * @returns DOM Element
     */
    createElement(tag: string, classNames: string[] = []) {
        const element = document.createElement(tag)
        for (const name of classNames) {
            if (name) element.classList.add(name)
        }
        return element
    }
}

/**
 * Class to handle communication between data model and UI 
 */
class Controller {
    model: Model    // this instance of the data model
    view: View      // this instance of view

    /**
     * Creating a instance of a constructor
     * @param model 
     */
    constructor(model: Model) {
        this.model = model
        this.view = new View((e: DirectoryFolder) => this.folderClickHandler(e))

        this.sizeValueChanged(this.model.currentSize) //update call for size label

        // binding all the handlers to view interaction
        this.view.bindImport(this.handleImport)
        this.view.bindCalc(this.handleCalc)
        this.view.bindNewFolder(this.newFolderClickedHandler)
        this.view.bindNewFile(this.newFileClickedHandler)
        this.view.bindReset(this.resetClickedHandler)
        this.view.bindSave(this.saveClickedHandler)
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder))
    }

    /**
     * Handler for adding a new file with specific size
     * @param value size of the new file
     */
    newFileClickedHandler = (value: number) => {
        (<DirectoryFolder>this.model.activeFolder).addChild(new DirectoryFile(value))
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder))
    }

    /**
     * Handler for adding a new folder to the current active folder
     */
    newFolderClickedHandler = () => {
        (<DirectoryFolder>this.model.activeFolder).addChild(new DirectoryFolder())
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder))
    }

    /**
     * Handler for reseting the directory
     */
    resetClickedHandler = () => {
        this.model.reset()
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder))
    }

    /**
     * Handler to save current directory to JSON
     */
    saveClickedHandler = () => {
        const JSONText = this.model.root.getJSON()
        var tempLink = document.createElement("a");
        var taBlob = new Blob([JSONText], { type: 'application/json' });
        tempLink.setAttribute('href', URL.createObjectURL(taBlob));
        tempLink.setAttribute('download', `directory.json`);
        tempLink.click();

        URL.revokeObjectURL(tempLink.href);
    }

    /**
     * Handler for selecting a new active folder
     * @param elem folder to set active
     */
    folderClickHandler(elem: DirectoryFolder) {
        this.model.setActiveFolder(elem)
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder))
    }

    /**
     * Handler for calculating directory size and updating view
     */
    handleCalc = () => {
        this.model.calcDirectorySize()
        this.sizeValueChanged(this.model.currentSize)
    }

    /**
     * Handler for importing directors in JSON Format as .json or .txt file
     * @param str JSON Object as string to be importet
     */
    handleImport = (str: string) => {
        const topElem = this.model.readNewDirectory(str)
        //this.sizeValueChanged(this.model.currentSize)
        this.model.setActiveFolder(<DirectoryFolder>this.model.root)
        this.view.BuildHtml(this.view.DirectoryElementToHTML(topElem, this.model.activeFolder))
    }

    /**
     * Call view update to display current directory size
     * @param value size to be displayed
     */
    sizeValueChanged = (value: number) => {
        this.view.updateValueLabel(value)
    }
}

// global insatnce of controller, initiating a model and a view
const app = new Controller(new Model())
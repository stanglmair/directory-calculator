# Directory Calculator

Directory Calculator is a small web-based tool to import or create directories containing folders and files, allowing one to recursively calculate the directory size.

## Getting started

Just open the `index.html` file in a browser of your choice. 
Tested on Google Chrome, Firefox, and Edge.\
Recommending Google Chrome


## Features

- importing directories from txt- ord JSON-Files (`testcase.json` includes the given test case)
- creating new or editing imported directories
- saving directories to JSON-File
- calculating the size of the current directory

### Design decisions / Ideas
- Folders and Files do not have names since they are unnecessary for intended use. Adding them later should be manageable.
- So far removing specific files and folders was not added but can be easily achieved later. The idea was to use the right mouse button. For now, only the entire directory can be removed.
- File size can not be edited. 
- Saving files using an Explorer pop-up would be a great idea
-  

## Project Info
The project was created in TypeScript and compiled into JavaScript. The UI was created using plain HTML and CSS. Icons were designed using GIMP and are licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1)

## Licenses

Directory Calculator © 2024 by Dennis Stanglmair is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).\
Economica Font is licensed under the [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).\
Roboto Mono Font is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
"use strict";
/**
 * ==========================================
 * App to calculate the size of an directory.
 * ==========================================
 * @author Dennis Stanglmair
 * @version 1.0.0
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Model that manages the directory and its data.
 */
var Model = /** @class */ (function () {
    /**
     * Initializing variables with default values.
     * Init root with an empty folder.
     */
    function Model() {
        this.root = new DirectoryFolder();
        this.currentSize = 0;
        this.activeFolder = this.root;
    }
    /**
     * Reset directory by overwriting the root with an empty folder.
     */
    Model.prototype.reset = function () {
        this.root = new DirectoryFolder();
        this.activeFolder = this.root;
    };
    /**
     * Recursivly calculating directory size by callin @function getDirSize from root element.
     */
    Model.prototype.calcDirectorySize = function () {
        this.currentSize = this.root.getDirSize();
    };
    /**
     * Importing a new directory in JSON-Format and calculating new @var currentSize using @function calcDirectorySize
     * @param str Directory in JSON Format as a String
     * @returns root as top level DirectoryElement
     */
    Model.prototype.readNewDirectory = function (str) {
        this.root = this.importJSON(str);
        this.calcDirectorySize();
        return this.root;
    };
    /**
     * Recursivly pasing JSON-File into Datastructure using @class DirectoryElemnt.
     * Throws an SyntaxError if JSON-Format is not as expected.
     * @param str Directory in JSON Format as a String
     * @returns top level DirectoryElement
     */
    Model.prototype.importJSON = function (str) {
        var obj = JSON.parse(str);
        if (obj.type == "DirFolder") {
            var folder = new DirectoryFolder();
            for (var _i = 0, _a = obj.children; _i < _a.length; _i++) {
                var child = _a[_i];
                folder.children.push(this.importJSON(child));
            }
            return folder;
        }
        else if (obj.type == "DirFile") {
            return new DirectoryFile(parseInt(obj.size));
        }
        else {
            throw new SyntaxError("Syntax Error occured reading JSON. type: " + obj.type + " not expected.");
        }
    };
    /**
     * Setting the active folder to a new DirectoryFolder.
     * @param elem The DirectoryFolder that is selected as active.
     */
    Model.prototype.setActiveFolder = function (elem) {
        this.activeFolder = elem;
    };
    return Model;
}());
/**
 * Abstract class that represents all directory elements and their common traits
 */
var DirectoryElement = /** @class */ (function () {
    function DirectoryElement() {
        this.size = 0; // init size with 0
    }
    return DirectoryElement;
}());
/**
 * Class to represent folders extending @class DirectoryElement.
 * Folder size is always set to 0; Managed by super() constructor
 */
var DirectoryFolder = /** @class */ (function (_super) {
    __extends(DirectoryFolder, _super);
    function DirectoryFolder() {
        var _this = _super.call(this) || this;
        _this.children = []; // init children as an empty list
        return _this;
    }
    /**
     * Calculates size using this DirectoryFolder Element as a root.
     * @returns size of this directory
     */
    DirectoryFolder.prototype.getDirSize = function () {
        var sum = 0;
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            sum += child.getDirSize(); // recursiv call to get size from children and summing them up
        }
        return sum;
    };
    /**
     * Adding a new child to this folder element
     * @param child child to add: can be DirectoryFolder or DirectoryFile
     */
    DirectoryFolder.prototype.addChild = function (child) {
        this.children.push(child);
    };
    /**
     * Recursivly concatenates the JSON representation of this folder, including the descendants
     * @returns This elements JSON representation as string
     */
    DirectoryFolder.prototype.getJSON = function () {
        var childrenObj = [];
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) { // iterate over all children
            var child = _a[_i];
            childrenObj.push(child.getJSON()); // recursive call to get JSON Representation of children.
        }
        var obj = {
            "type": 'DirFolder', // type to easily differentiate between folder and files
            children: childrenObj, // list of children objects
        };
        return JSON.stringify(obj);
    };
    return DirectoryFolder;
}(DirectoryElement));
/**
 * Class to represent files extending @class DirectoryElement.
 * File size is reset by constructor.
 */
var DirectoryFile = /** @class */ (function (_super) {
    __extends(DirectoryFile, _super);
    function DirectoryFile(size) {
        var _this = _super.call(this) || this;
        _this.size = size; // setting size
        return _this;
    }
    /**
     * Getter for this files size. Used as an end to the recusive call.
     * @returns size of this file.
     */
    DirectoryFile.prototype.getDirSize = function () {
        return this.size;
    };
    /**
     * Getter for JSON representation of this file.
     * @returns This elements JSON representation as string
     */
    DirectoryFile.prototype.getJSON = function () {
        return JSON.stringify({
            'type': 'DirFile',
            size: this.size,
        });
    };
    return DirectoryFile;
}(DirectoryElement));
/**
 * Class to handle UI based actions
 */
var View = /** @class */ (function () {
    function View(clickHandler) {
        var _a;
        this.folderClickHandler = clickHandler; // clickhandler for click on folder, changing the new active folder
        // Building the UI for the right calculator module inclusive JSON importer
        this.app = document.getElementById('root');
        // JSON Headline and container
        this.JSONHeader = this.createElement('h2', ["sub-module-header"]);
        this.JSONHeader.textContent = "JSON-Reader";
        // JSON import tools
        this.ImportButton = this.createElement('button', ["button"]);
        this.ImportButton.textContent = "Import";
        // Calculate Size tools
        this.ClacButton = this.createElement('button', ["button"]);
        this.ClacButton.textContent = "Calculate Size";
        // Display size tools
        this.valueLabel = this.createElement('div', ["label"]);
        this.valueLabel.textContent = "0";
        // Upload file tools
        this.upload = this.createElement('input');
        this.upload.type = "file";
        this.upload.accept = ".txt, .JSON, .json";
        // Error line components
        this.ErrorLine = this.createElement("div", ['error-line']);
        this.ErrorLine.textContent = '';
        // JSON import components
        this.JSONContainter = this.createElement('div', ['sub-module-container']);
        this.JSONTopBar = this.createElement('div', ['sub-module-toolbar']);
        this.JSONBottomBar = this.createElement('div', ['sub-module-toolbar-bottom']);
        this.JSONTopBar.append(this.upload);
        this.JSONBottomBar.append(this.ImportButton);
        this.JSONContainter.append(this.JSONHeader, this.JSONTopBar, this.JSONBottomBar, this.ErrorLine);
        // Size display components
        this.LabelBar = this.createElement('div', ['sub-module-toolbar']);
        var Label = this.createElement('span', ['label-bar-label']);
        Label.textContent = "Directory Size:";
        this.LabelBar.append(Label, this.valueLabel);
        // Calculating size components
        var calcBar = this.createElement('div', ['sub-module-toolbar']);
        calcBar.append(this.ClacButton);
        var calcHeader = this.createElement('h2', ["sub-module-header", "calcHeader"]);
        calcHeader.textContent = "Calculator";
        this.app.append(this.JSONContainter, calcHeader, this.LabelBar, calcBar);
        // Building UI for left directory editor and display module
        this.editor = document.getElementById('rootLeft');
        this.editor.classList.add('container');
        // Input for setting the size of a potentially new file
        this.sizeValue = this.createElement('input', ['number-input']);
        this.sizeValue.type = "number";
        this.sizeValue.min = "0";
        this.sizeValue.max = "999";
        this.sizeValue.value = "1";
        this.sizeValue.step = "1";
        (_a = document.getElementById("size-container")) === null || _a === void 0 ? void 0 : _a.append(this.sizeValue);
    }
    /**
     * Displaying HTML structure for directory
     * @param elem HTML representation of directory as @class Element
     */
    View.prototype.BuildHtml = function (elem) {
        this.editor.innerHTML = ""; // resetting structure
        this.editor.append(elem); // loading new structure
    };
    /**
     * Get the HTML representation of @param elem
     * @param elem Element to be translated to HTML
     * @param activeElem currently active element needed to adjust color indicator
     * @returns recursivly built HTML representation of @param elem
     */
    View.prototype.DirectoryElementToHTML = function (elem, activeElem) {
        var _this = this;
        var html = this.createElement('div', ['dir-row']); // class dir-row has an left margin, so they get indented, compared to their parent 
        var img = this.createElement('img', ['dir-icon']);
        html.append(img);
        if (elem instanceof DirectoryFolder) { // create folder html
            img.src = "res/folder.png";
            img.classList.add("tool-icon");
            if (elem == activeElem)
                img.classList.add('active-folder');
            for (var _i = 0, _a = elem.children; _i < _a.length; _i++) {
                var child = _a[_i];
                html.append(this.DirectoryElementToHTML(child, activeElem));
            }
            img.addEventListener('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                _this.folderClickHandler(elem);
            });
        }
        else if (elem instanceof DirectoryFile) { // create file html; else possible.
            img.src = "res/file.png";
            var s = this.createElement('span', ['file-size-label']);
            s.append("(".concat(elem.size, ")"));
            html.append(s); // addinf file size in braces behind every file
        }
        return html;
    };
    /**
     * Updating the label to display the current directory size.
     * @param value size value to be displayed
     */
    View.prototype.updateValueLabel = function (value) {
        this.valueLabel.innerHTML = value.toString();
    };
    /**
     * Binding handler to new folder icon click.
     * @param handler newFolderClickedHandler
     */
    View.prototype.bindNewFolder = function (handler) {
        var newFolderButton = document.getElementById("newFolderIcon");
        newFolderButton === null || newFolderButton === void 0 ? void 0 : newFolderButton.addEventListener('click', function (event) {
            event.preventDefault();
            handler();
        });
    };
    /**
     * Binding handler to new file click.
     * @param handler newFileClickedHandler
     */
    View.prototype.bindNewFile = function (handler) {
        var _this = this;
        var newFileButton = document.getElementById("newFileIcon");
        newFileButton === null || newFileButton === void 0 ? void 0 : newFileButton.addEventListener('click', function (event) {
            event.preventDefault();
            var finalValue = Math.floor(parseFloat(_this.sizeValue.value));
            if (isNaN(finalValue))
                return;
            if (finalValue < 0)
                finalValue = 0;
            _this.sizeValue.value = finalValue.toString();
            handler(finalValue);
        });
    };
    /**
     * Binding handler to reset click.
     * @param handler resetClickedHandler
     */
    View.prototype.bindReset = function (handler) {
        var resetButton = document.getElementById("resetIcon");
        resetButton === null || resetButton === void 0 ? void 0 : resetButton.addEventListener('click', function (event) {
            event.preventDefault();
            handler();
        });
    };
    /**
     * Binding handler to save click.
     * @param handler saveClickedHandler
     */
    View.prototype.bindSave = function (handler) {
        var saveButton = document.getElementById("saveIcon");
        saveButton === null || saveButton === void 0 ? void 0 : saveButton.addEventListener('click', function (event) {
            event.preventDefault();
            handler();
        });
    };
    /**
     * Binding handler to import click.
     * @param handler handleImport
     */
    View.prototype.bindImport = function (handler) {
        var _this = this;
        this.ImportButton.addEventListener('click', function (event) {
            event.preventDefault();
            var fileToLoad = _this.upload.files[0];
            if (!fileToLoad) { // handling no file selected
                _this.ErrorLine.textContent = "There is no file selected! Please select a .txt or .json File.";
                throw new Error("No File selected!");
            }
            else if (fileToLoad.type != 'text/plain' && fileToLoad.type != 'application/json') { // handling wrong file type selected
                _this.ErrorLine.textContent = "The Calculator does not support Files of type " + fileToLoad.type + "! Please select a .txt or .json File.";
                throw Error("File Type does not match!");
            }
            else { // cleansing error line
                _this.ErrorLine.textContent = "";
            }
            var fileReader = new FileReader();
            // read file and sending it to handler to import it
            fileReader.onload = function (fileLoadedEvent) {
                console.log(fileLoadedEvent.target.result);
                handler(fileLoadedEvent.target.result);
            };
            fileReader.readAsText(fileToLoad, "UTF-8");
        });
    };
    /**
     * Binding handler to calc button click.
     * @param handler handleCalc
     */
    View.prototype.bindCalc = function (handler) {
        this.ClacButton.addEventListener('click', function (event) {
            event.preventDefault();
            handler();
        });
    };
    /**
     * Create an Element for DOM Tree
     * @param tag Tag of element
     * @param classNames css class names as list
     * @returns DOM Element
     */
    View.prototype.createElement = function (tag, classNames) {
        if (classNames === void 0) { classNames = []; }
        var element = document.createElement(tag);
        for (var _i = 0, classNames_1 = classNames; _i < classNames_1.length; _i++) {
            var name_1 = classNames_1[_i];
            if (name_1)
                element.classList.add(name_1);
        }
        return element;
    };
    return View;
}());
/**
 * Class to handle communication between data model and UI
 */
var Controller = /** @class */ (function () {
    /**
     * Creating a instance of a constructor
     * @param model
     */
    function Controller(model) {
        var _this = this;
        /**
         * Handler for adding a new file with specific size
         * @param value size of the new file
         */
        this.newFileClickedHandler = function (value) {
            _this.model.activeFolder.addChild(new DirectoryFile(value));
            _this.view.BuildHtml(_this.view.DirectoryElementToHTML(_this.model.root, _this.model.activeFolder));
        };
        /**
         * Handler for adding a new folder to the current active folder
         */
        this.newFolderClickedHandler = function () {
            _this.model.activeFolder.addChild(new DirectoryFolder());
            _this.view.BuildHtml(_this.view.DirectoryElementToHTML(_this.model.root, _this.model.activeFolder));
        };
        /**
         * Handler for reseting the directory
         */
        this.resetClickedHandler = function () {
            _this.model.reset();
            _this.view.BuildHtml(_this.view.DirectoryElementToHTML(_this.model.root, _this.model.activeFolder));
        };
        /**
         * Handler to save current directory to JSON
         */
        this.saveClickedHandler = function () {
            var JSONText = _this.model.root.getJSON();
            var tempLink = document.createElement("a");
            var taBlob = new Blob([JSONText], { type: 'application/json' });
            tempLink.setAttribute('href', URL.createObjectURL(taBlob));
            tempLink.setAttribute('download', "directory.json");
            tempLink.click();
            URL.revokeObjectURL(tempLink.href);
        };
        /**
         * Handler for calculating directory size and updating view
         */
        this.handleCalc = function () {
            _this.model.calcDirectorySize();
            _this.sizeValueChanged(_this.model.currentSize);
        };
        /**
         * Handler for importing directors in JSON Format as .json or .txt file
         * @param str JSON Object as string to be importet
         */
        this.handleImport = function (str) {
            var topElem = _this.model.readNewDirectory(str);
            //this.sizeValueChanged(this.model.currentSize)
            _this.model.setActiveFolder(_this.model.root);
            _this.view.BuildHtml(_this.view.DirectoryElementToHTML(topElem, _this.model.activeFolder));
        };
        /**
         * Call view update to display current directory size
         * @param value size to be displayed
         */
        this.sizeValueChanged = function (value) {
            _this.view.updateValueLabel(value);
        };
        this.model = model;
        this.view = new View(function (e) { return _this.folderClickHandler(e); });
        this.sizeValueChanged(this.model.currentSize); //update call for size label
        // binding all the handlers to view interaction
        this.view.bindImport(this.handleImport);
        this.view.bindCalc(this.handleCalc);
        this.view.bindNewFolder(this.newFolderClickedHandler);
        this.view.bindNewFile(this.newFileClickedHandler);
        this.view.bindReset(this.resetClickedHandler);
        this.view.bindSave(this.saveClickedHandler);
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder));
    }
    /**
     * Handler for selecting a new active folder
     * @param elem folder to set active
     */
    Controller.prototype.folderClickHandler = function (elem) {
        this.model.setActiveFolder(elem);
        this.view.BuildHtml(this.view.DirectoryElementToHTML(this.model.root, this.model.activeFolder));
    };
    return Controller;
}());
// global insatnce of controller, initiating a model and a view
var app = new Controller(new Model());
//# sourceMappingURL=calcModule.js.map